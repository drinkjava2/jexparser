# jExParser 一个简单的Java表达式解析求值程序 
在jSqlBox项目开发时写的一个简单的Java表达式解析求值程序，支持变量和参数，现将源码抽取出来，没有什么特别优点，唯一的长处就是简单，直接中缀求值，仅三个文件800行源码，无第三方库依赖。因为比较简单而且通常用于二次开发，所以没有做成包发布，如有项目中需要用到把源码拷过去就可以了。
两种用法如下(或参考单元测试):
```
  Map<String, Object> keywords = new HashMap<String, Object>();
  keywords.put("USERNAME", "Tom");
  keywords.put("ID", "001");
  ExParser parser=new ExParser();
  
  //解释执行，速度较慢，此例为每秒10万次(i7 CPU)
  Assert.assertEquals(true, parser.doParse(keywords,
    "(1+2)*3/4>0.1/(9+?) and (userName equals ?) or id equals ?", 100, "Tom", "001"));
    
  //编译执行,速度约为解释执行的10倍，此例为每秒100万次
  ExpItem[] expItems = parser.compile(
    "(1+2)*3/4>0.1/(9+?) and (userName equals ?) and id equals ?");
  Assert.assertEquals(true, parser.doParse(expItems, keywords, 100, "Tom", "001"));   
    
```
ExParser类的doParse()方法解析一个表达式，返回类型可能为Boolean、Long、Double、String、null之一。
目前支持以下操作符及字符串函数：
```
>  <  =  >=  <=  
+  -  *  /  
or  and  not  
'  ( )  ?  0~9 . 
equals  equalsIgnoreCase  contains  containsIgnoreCase  
startWith  startWithIgnoreCase  endWith  endWithIgnoreCase
```




