/*
 * Copyright (C) 2016 Yong Zhu.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by
 * applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 * OF ANY KIND, either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 */
package coveragetest;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.github.drinkjava2.jexparser.ExParser;
import com.github.drinkjava2.jexparser.ExParser.ExpItem;

/**
 * Coverage test for TinyParser
 * 
 * @author Yong Zhu (Yong9981@gmail.com)
 * @since 1.0.0
 */
public class ExParserTest {
	public static final ExParser instance = new ExParser();

	@Test
	public void testMath() {
		Assert.assertEquals(-48L, instance.doParse(null, " -(9-(3+3))*8 -24 "));
		Assert.assertEquals(-47L, instance.doParse(null, "1-(9-(3+9/3))*8 -24"));
		Assert.assertEquals(true, instance.doParse(null, "1.0/100000000000=(2-1.0)/100000000000"));
	}

	@Test
	public void testBoolen() {
		Assert.assertEquals(true, instance.doParse(null, " not not ( not  not(true))"));

		Assert.assertEquals(true, instance.doParse(null, "true"));

		Assert.assertEquals(false, instance.doParse(null, "false"));

		Assert.assertEquals(false, instance.doParse(null, "not(true)"));

		Assert.assertEquals(true, instance.doParse(null, "not(false)"));

		String s = "(1+1)=2 and not(1=2) and not(false) and true and (1=1 or 1=2) and  'Tom' equalsIgnoreCase 'tom' ";
		Assert.assertEquals(true, instance.doParse(null, s));
	}

	@Test
	public void testStringFunctions() {
		Assert.assertEquals(true, instance.doParse(null, "'Tom' = 'Tom'"));
		Assert.assertEquals(false, instance.doParse(null, "'Tom' = 'tom'"));
		Assert.assertEquals(true, instance.doParse(null, "'Tom' <> 'tom'"));

		Assert.assertEquals(true, instance.doParse(null, "'Tom' equals 'Tom'"));
		Assert.assertEquals(false, instance.doParse(null, "'Tom' equals 'tom'"));
		Assert.assertEquals(true, instance.doParse(null, "'Tom' equalsIgnoreCase 'tom'"));

		Assert.assertEquals(true, instance.doParse(null, "'Tom' contains 'om'"));
		Assert.assertEquals(false, instance.doParse(null, "'Tom' contains 'tom'"));
		Assert.assertEquals(true, instance.doParse(null, "'Tom' containsIgnoreCase 'tom'"));

		Assert.assertEquals(true, instance.doParse(null, "'Tom' startWith 'To'"));
		Assert.assertEquals(false, instance.doParse(null, "'Tom' startWith 'to'"));
		Assert.assertEquals(true, instance.doParse(null, "'Tom' startWithIgnoreCase 'to'"));

		Assert.assertEquals(true, instance.doParse(null, "'Tom' endWith 'om'"));
		Assert.assertEquals(false, instance.doParse(null, "'Tom' endWith 'OM'"));
		Assert.assertEquals(true, instance.doParse(null, "'Tom' endWithIgnoreCase 'OM'"));
	}

	@Test
	public void testNull() {
		Assert.assertEquals(null, instance.doParse(null, "  "));
		Assert.assertEquals(null, instance.doParse(null, " "));
		Assert.assertEquals(false, instance.doParse(null, "null = 'abc'"));
		Assert.assertEquals(true, instance.doParse(null, "null <> 'abc'"));
		Assert.assertEquals(false, instance.doParse(null, "null >6"));
		Assert.assertEquals(false, instance.doParse(null, "6 >=null"));

		Assert.assertEquals(true, instance.doParse(null, "null is null"));

		Map<String, Object> preset = new HashMap<String, Object>();
		preset.put("FOO", null);
		Assert.assertEquals(true, instance.doParse(preset, "FOO is NULL"));
	}

	@Test
	public void testParams() {
		Map<String, Object> keywords = new HashMap<String, Object>();
		keywords.put("USERNAME", "Tom");
		keywords.put("ID", "001");
		Assert.assertEquals(true, instance.doParse(keywords, "userName equals ? and id equals ?", "Tom", "001"));
		Assert.assertEquals(false, instance.doParse(keywords, "userName equals ? and id equals ?", "Tom", "002"));
	}

	@Test
	public void testVariants() {
		Map<String, Object> keywords = new HashMap<String, Object>();
		keywords.put("FOO", 123);
		keywords.put("BAR", "456");
		Assert.assertEquals(true, instance.doParse(keywords, "FOO = 123 and BAR='456'"));
		Assert.assertEquals(true, instance.doParse(keywords, "foo = 123 and bar equals '456'"));
	}

	@Test
	public void testDemo() {
		Map<String, Object> keywords = new HashMap<String, Object>();
		keywords.put("USERNAME", "Tom");
		keywords.put("ID", "001");
		Assert.assertEquals(true, new ExParser().doParse(keywords,
				"(1+2)*3/4>0.1/(9+?) and (userName equals ?) or id equals ?", 100, "Tom", "001"));
	}

	@Test
	public void testSpeed() {
		int repeatTimes=100000;
		Map<String, Object> keywords = new HashMap<String, Object>();
		keywords.put("FOO", 123);
		keywords.put("BAR", "456");
		keywords.put("USERNAME", "Tom");
		keywords.put("ID", "001");
		long start = System.currentTimeMillis();
		for (int i = 0; i < repeatTimes; i++)
			Assert.assertEquals(true,
					instance.doParse(keywords,
							"(1+2)*3/4>0.1/(9+?) and (userName equals ?) and id equals ?  ",
							100, "Tom", "001"));
		Double secondsUsedExplain = (System.currentTimeMillis() - start) / 1000.0;
		System.out.println("Parser Explain execute "+repeatTimes+" times cost " + secondsUsedExplain + "s");

		start = System.currentTimeMillis();
		ExpItem[] itmes = instance.compile(
				"(1+2)*3/4>0.1/(9+?) and (userName equals ?) and id equals ?  "); 
		System.out.println("==============================");
		for (int i = 0; i < repeatTimes; i++)
			Assert.assertEquals(true, instance.doParse(itmes, keywords, 100, "Tom", "001"));  
		Double secondsUsedCompiled = (System.currentTimeMillis() - start) / 1000.0;
		System.out.println("Parser compiled execute "+repeatTimes+" times cost " + secondsUsedCompiled + "s");

		System.out.println("Compiled execute speed is " + secondsUsedExplain / secondsUsedCompiled
				+ " times compare to explain execute");
	}

}
